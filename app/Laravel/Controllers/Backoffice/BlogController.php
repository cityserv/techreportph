<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Blog;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\BlogRequest;
use App\Laravel\Requests\Backoffice\EditBlogRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class BlogController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "blog";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "blog";
		$this->data['status'] = ['' => "Choose Status",'draft' => "Draft", 'published' => "Published"];
	}

	public function index () {
		$this->data['blog'] = Blog::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (BlogRequest $request) {
		try {
			$new_blog = new Blog;
			$new_blog->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/blog');
				$new_blog->path = $upload["path"];
				$new_blog->directory = $upload["directory"];
				$new_blog->filename = $upload["filename"];
			}

			if($new_blog->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New partner has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}	
}
	public function edit ($id = NULL) {
		$blog = Blog::find($id);

		if (!$blog) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['blog'] = $blog;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditBlogRequest $request, $id = NULL) {
		try {
			$blog = Blog::find($id);

			if (!$blog) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$blog->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/blog');
				if($upload){	
					if (File::exists("{$blog->directory}/{$blog->filename}")){
						File::delete("{$blog->directory}/{$blog->filename}");
					}
					if (File::exists("{$blog->directory}/resized/{$blog->filename}")){
						File::delete("{$blog->directory}/resized/{$blog->filename}");
					}
					if (File::exists("{$blog->directory}/thumbnails/{$blog->filename}")){
						File::delete("{$blog->directory}/thumbnails/{$blog->filename}");
					}
				}
				
				$blog->path = $upload["path"];
				$blog->directory = $upload["directory"];
				$blog->filename = $upload["filename"];
			}

			if($blog->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A partner has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$blog = Blog::find($id);

			if (!$blog) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$blog->directory}/{$blog->filename}")){
				File::delete("{$blog->directory}/{$blog->filename}");
			}
			if (File::exists("{$blog->directory}/resized/{$blog->filename}")){
				File::delete("{$blog->directory}/resized/{$blog->filename}");
			}
			if (File::exists("{$blog->directory}/thumbnails/{$blog->filename}")){
				File::delete("{$blog->directory}/thumbnails/{$blog->filename}");
			}

			if($blog->save() AND $blog->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A partner has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}