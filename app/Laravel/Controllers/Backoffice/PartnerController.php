<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Partner;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\PartnerRequest;
use App\Laravel\Requests\Backoffice\EditPartnerRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class PartnerController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Partners";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "partners";
		$this->data['types'] = [''=>'Choose partner type','bronze'=>"Bronze",'platinum'=>"Platinum"];
	}

	public function index () {
		$this->data['partners'] = Partner::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (PartnerRequest $request) {
		try {
			$new_partners = new Partner;
			$new_partners->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/partners');
				$new_partners->path = $upload["path"];
				$new_partners->directory = $upload["directory"];
				$new_partners->filename = $upload["filename"];
			}

			if($new_partners->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New partner has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$partners = Partner::find($id);

		if (!$partners) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['partner'] = $partners;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditPartnerRequest $request, $id = NULL) {
		try {
			$partners = Partner::find($id);

			if (!$partners) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$partners->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/partners');
				if($upload){	
					if (File::exists("{$partners->directory}/{$partners->filename}")){
						File::delete("{$partners->directory}/{$partners->filename}");
					}
					if (File::exists("{$partners->directory}/resized/{$partners->filename}")){
						File::delete("{$partners->directory}/resized/{$partners->filename}");
					}
					if (File::exists("{$partners->directory}/thumbnails/{$partners->filename}")){
						File::delete("{$partners->directory}/thumbnails/{$partners->filename}");
					}
				}
				
				$partners->path = $upload["path"];
				$partners->directory = $upload["directory"];
				$partners->filename = $upload["filename"];
			}

			if($partners->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A partner has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$partners = Partner::find($id);

			if (!$partners) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$partners->directory}/{$partners->filename}")){
				File::delete("{$partners->directory}/{$partners->filename}");
			}
			if (File::exists("{$partners->directory}/resized/{$partners->filename}")){
				File::delete("{$partners->directory}/resized/{$partners->filename}");
			}
			if (File::exists("{$partners->directory}/thumbnails/{$partners->filename}")){
				File::delete("{$partners->directory}/thumbnails/{$partners->filename}");
			}

			if($partners->save() AND $partners->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A partner has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}