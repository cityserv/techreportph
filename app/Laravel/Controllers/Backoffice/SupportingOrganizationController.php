<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\SupportingOrganization;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\SupportingOrganizationRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class SupportingOrganizationController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Supporting Organization";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "supporting_organization";
	}

	public function index () {
		$this->data['organizations'] = SupportingOrganization::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (SupportingOrganizationRequest $request) {
		try {
			$new_organization = new SupportingOrganization;
			$new_organization->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/organization');
				$new_organization->path = $upload["path"];
				$new_organization->directory = $upload["directory"];
				$new_organization->filename = $upload["filename"];
			}

			if($new_organization->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New organization has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$organization = SupportingOrganization::find($id);

		if (!$organization) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['organization'] = $organization;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (SupportingOrganizationRequest $request, $id = NULL) {
		try {
			$organization = SupportingOrganization::find($id);

			if (!$organization) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$organization->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/organization');
				if($upload){	
					if (File::exists("{$organization->directory}/{$organization->filename}")){
						File::delete("{$organization->directory}/{$organization->filename}");
					}
					if (File::exists("{$organization->directory}/resized/{$organization->filename}")){
						File::delete("{$organization->directory}/resized/{$organization->filename}");
					}
					if (File::exists("{$organization->directory}/thumbnails/{$organization->filename}")){
						File::delete("{$organization->directory}/thumbnails/{$organization->filename}");
					}
				}
				
				$organization->path = $upload["path"];
				$organization->directory = $upload["directory"];
				$organization->filename = $upload["filename"];
			}

			if($organization->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A organization has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$organization = SupportingOrganization::find($id);

			if (!$organization) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$organization->directory}/{$organization->filename}")){
				File::delete("{$organization->directory}/{$organization->filename}");
			}
			if (File::exists("{$organization->directory}/resized/{$organization->filename}")){
				File::delete("{$organization->directory}/resized/{$organization->filename}");
			}
			if (File::exists("{$organization->directory}/thumbnails/{$organization->filename}")){
				File::delete("{$organization->directory}/thumbnails/{$organization->filename}");
			}

			if($organization->save() AND $organization->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A organization has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}