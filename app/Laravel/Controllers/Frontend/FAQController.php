<?php namespace App\Laravel\Controllers\Frontend;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\FAQ;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Frontend\FAQRequest;
use App\Laravel\Requests\Frontend\EditFAQRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class FAQController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Frequently Asked Questions";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "pages.faq";
	}

	public function create () {
		return view('frontend.'.$this->data['route_file']);
	}

	public function store (FAQRequest $request) {
		try {
			$new_faq = new FAQ;
			$new_faq->fill($request->all());

			if($new_faq->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New faq has been added.");
				return redirect()->route('frontend.faq');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}