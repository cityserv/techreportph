<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class ImageSliderRequest extends RequestManager{

	public function rules(){

		$rules = [
			'title' => "required",
			'sub_title' => "required",
			'order' => "required",
			'file' => "required|image",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}
}