<?php namespace App\Laravel\Requests\Frontend;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class EditFAQRequest extends RequestManager{

	public function rules(){

		$rules = [
			'question' => "required",
			'answer' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}
}