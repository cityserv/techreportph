<?php


/**
 *
 * ------------------------------------
 * Backoffice Routes
 * ------------------------------------
 *
 */

Route::group(

	array(
		'as' => "backoffice.",
		'prefix' => "admin",
		'namespace' => "Backoffice",
	),

	function() {

		$this->group(['as'=>"auth.", 'middleware' => ["web","backoffice.guest"]], function(){
			$this->get('login',['as' => "login",'uses' => "AuthController@login"]);
			$this->post('login',['uses' => "AuthController@authenticate"]);
		});

		$this->group(['middleware' => "backoffice.auth"], function(){

			$this->get('/',['as' => "dashboard",'uses' => "DashboardController@index"]);
			$this->get('voters',['as' => "voters",'uses' => "VotersController@index"]);
			$this->get('logout',['as' => "logout",'uses' => "AuthController@destroy"]);


			$this->group(['prefix' => "employees", 'as' => "employee.",'middleware' => "backoffice.super_user_only"], function () {
				$this->get('/',['as' => "index", 'uses' => "EmployeeController@index"]);
				$this->get('create',['as' => "create", 'uses' => "EmployeeController@create"]);
				$this->post('create',['uses' => "EmployeeController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "EmployeeController@edit"]);
				$this->post('edit/{id?}',['uses' => "EmployeeController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "EmployeeController@destroy"]);
			});

			$this->group(['prefix' => "news", 'as' => "news."], function () {
				$this->get('/',['as' => "index", 'uses' => "NewsController@index"]);
				$this->get('create',['as' => "create", 'uses' => "NewsController@create"]);
				$this->post('create',['uses' => "NewsController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "NewsController@edit"]);
				$this->post('edit/{id?}',['uses' => "NewsController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "NewsController@destroy"]);
			});

			$this->group(['prefix' => "partners", 'as' => "partners."], function () {
				$this->get('/',['as' => "index", 'uses' => "PartnerController@index"]);
				$this->get('create',['as' => "create", 'uses' => "PartnerController@create"]);
				$this->post('create',['uses' => "PartnerController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "PartnerController@edit"]);
				$this->post('edit/{id?}',['uses' => "PartnerController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "PartnerController@destroy"]);
			});

			$this->group(['prefix' => "users", 'as' => "users."], function(){
				$this->get('/',['as' => "index",'uses' => "UserController@index"]);
				$this->get('create',['as' => "create",'uses' => "UserController@create"]);
				$this->post('create',['uses' => "UserController@store"]);
				$this->get('edit/{id?}',['as' => "edit",'uses' => "UserController@edit"]);
				$this->post('edit/{id?}',['uses' => "UserController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "UserController@destroy"]);
			});


			$this->group(['prefix' => "events", 'as' => "events."], function () {
				$this->get('/',['as' => "index", 'uses' => "EventController@index"]);
				$this->get('create',['as' => "create", 'uses' => "EventController@create"]);
				$this->post('create',['uses' => "EventController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "EventController@edit"]);
				$this->post('edit/{id?}',['uses' => "EventController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "EventController@destroy"]);
			});
			
			$this->group(['prefix' => "page-content", 'as' => "page_content."], function () {
				$this->get('/',['as' => "index", 'uses' => "PageContentController@index"]);
				$this->get('create',['as' => "create", 'uses' => "PageContentController@create"]);
				$this->post('create',['uses' => "PageContentController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "PageContentController@edit"]);
				$this->post('edit/{id?}',['uses' => "PageContentController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "PageContentController@destroy"]);
			});
			
			
			$this->group(['prefix' => "social-links", 'as' => "social_links."], function () {
				$this->get('/',['as' => "index", 'uses' => "SocialLinkController@index"]);
				$this->get('create',['as' => "create", 'uses' => "SocialLinkController@create"]);
				$this->post('create',['uses' => "SocialLinkController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "SocialLinkController@edit"]);
				$this->post('edit/{id?}',['uses' => "SocialLinkController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "SocialLinkController@destroy"]);
			});
			
			$this->group(['prefix' => "contact-inquiries", 'as' => "contact_inquiry."], function () {
				$this->get('/',['as' => "index", 'uses' => "ContactInquiryController@index"]);
				$this->get('create',['as' => "create", 'uses' => "ContactInquiryController@create"]);
				$this->post('create',['uses' => "ContactInquiryController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ContactInquiryController@edit"]);
				$this->post('edit/{id?}',['uses' => "ContactInquiryController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ContactInquiryController@destroy"]);
			});

			$this->group(['prefix' => "subscribers", 'as' => "subscribers."], function () {
				$this->get('/',['as' => "index", 'uses' => "SubscribersController@index"]);
			});

			$this->group(['prefix' => "question", 'as' => "question."], function () {
				$this->get('/',['as' => "index", 'uses' => "QuestionController@index"]);
			});

			$this->group(['prefix' => "logo", 'as' => "logo."], function () {
				$this->get('/',['as' => "index", 'uses' => "LogoController@edit"]);
				$this->post('/',['uses' => "LogoController@update"]);
			});

			$this->group(['prefix' => "image_slider", 'as' => "image_slider."], function () {
				$this->get('/',['as' => "index", 'uses' => "ImageSliderController@index"]);
				$this->get('create',['as' => "create", 'uses' => "ImageSliderController@create"]);
				$this->post('create',['uses' => "ImageSliderController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ImageSliderController@edit"]);
				$this->post('edit/{id?}',['uses' => "ImageSliderController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ImageSliderController@destroy"]);
			});
			
			$this->group(['prefix' => "faq", 'as' => "faq."], function () {
				$this->get('/',['as' => "index", 'uses' => "FAQController@index"]);
				$this->get('create',['as' => "create", 'uses' => "FAQController@create"]);
				$this->post('create',['uses' => "FAQController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "FAQController@edit"]);
				$this->post('edit/{id?}',['uses' => "FAQController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "FAQController@destroy"]);
			});

			$this->group(['prefix' => "testimonials", 'as' => "testimonials."], function () {
				$this->get('/',['as' => "index", 'uses' => "TestimonialController@index"]);
				$this->get('create',['as' => "create", 'uses' => "TestimonialController@create"]);
				$this->post('create',['uses' => "TestimonialController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "TestimonialController@edit"]);
				$this->post('edit/{id?}',['uses' => "TestimonialController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "TestimonialController@destroy"]);
			});

			$this->group(['prefix' => "awards", 'as' => "awards."], function () {
				$this->get('/',['as' => "index", 'uses' => "AwardsController@index"]);
			});

			$this->group(['prefix' => "blog", 'as' => "blog."], function () {
				$this->get('/',['as' => "index", 'uses' => "BlogController@index"]);
				$this->get('create',['as' => "create", 'uses' => "BlogController@create"]);
				$this->post('create',['uses' => "BlogController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "BlogController@edit"]);
				$this->post('edit/{id?}',['uses' => "BlogController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "BlogController@destroy"]);
			});

			$this->group(['prefix' => "albums", 'as' => "albums."], function () {
				$this->get('/',['as' => "index", 'uses' => "AlbumController@index"]);
				$this->get('create',['as' => "create", 'uses' => "AlbumController@create"]);
				$this->post('create',['uses' => "AlbumController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "AlbumController@edit"]);
				$this->post('edit/{id?}',['uses' => "AlbumController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "AlbumController@destroy"]);
			});

			$this->group(['prefix' => "gallery", 'as' => "gallery."], function () {
				$this->get('/',['as' => "index", 'uses' => "GalleryController@index"]);
				$this->get('create',['as' => "create", 'uses' => "GalleryController@create"]);
				$this->post('create',['uses' => "GalleryController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "GalleryController@edit"]);
				$this->post('edit/{id?}',['uses' => "GalleryController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "GalleryController@destroy"]);
			});

			$this->group(['prefix' => "about", 'as' => "about."], function () {
				$this->get('/',['as' => "index", 'uses' => "AboutController@edit"]);
				$this->post('/',['uses' => "AboutController@update"]);
			});

			$this->group(['prefix' => "logo", 'as' => "logo."], function () {
				$this->get('/',['as' => "index", 'uses' => "LogoController@edit"]);
				$this->post('/',['uses' => "LogoController@update"]);
			});		

			$this->group(['prefix' => "category", 'as' => "category."], function () {
				$this->get('/',['as' => "index", 'uses' => "NewsCategoryController@index"]);
				$this->get('create',['as' => "create", 'uses' => "NewsCategoryController@create"]);
				$this->post('create',['uses' => "NewsCategoryController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "NewsCategoryController@edit"]);
				$this->post('edit/{id?}',['uses' => "NewsCategoryController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "NewsCategoryController@destroy"]);
			});

		});
	}
);