<?php


/**
 *
 * ------------------------------------
 * Frontend Routes
 * ------------------------------------
 *
 */

$this->group(

	array(
		'as' => "frontend.",
		'namespace' => "Frontend",
	),

	function() {

		$this->get('/',['as' => "index",'uses' => "PageController@index"]);
		$this->get('about',['as' => "about",'uses' => "PageController@about"]);
		$this->get('contact',['as' => "contact",'uses' => "PageController@contact"]);
		$this->post('contact',['uses' => "PageController@contactstore"]);		
		$this->get('faq',['as' => "faq",'uses' => "PageController@faq"]);
		$this->post('faq',['uses' => "PageController@faqstore"]);


		$this->group(['prefix' => "partners", 'as' => "partners."], function () {
			$this->get('/{type}',['as' => "index", 'uses' => "PartnerController@index"]);
		});

		$this->group(['prefix' => "events", 'as' => "events."], function () {
			$this->get('/',['as' => "index", 'uses' => "EventsController@index"]);
			$this->get('show/{id}',['as' => "show", 'uses' => "EventsController@show"]);
		});

		$this->group(['prefix' => "news", 'as' => "news."], function () {
			$this->get('news/{id}',['as' => "news", 'uses' => "NewsController@news"]);
			$this->get('show/{id}',['as' => "show", 'uses' => "NewsController@show"]);
		});

		$this->group(['prefix' => "gallery", 'as' => "gallery."], function () {
			$this->get('/',['as' => "index", 'uses' => "GalleryController@index"]);
			$this->get('show/{id}',['as' => "show", 'uses' => "GalleryController@show"]);
		});
	}
);