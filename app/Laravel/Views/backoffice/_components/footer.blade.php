
<!-- Footer -->
<div class="footer text-muted">
	&copy; {{ Carbon::now()->format("Y") }}. <a href="#">{{env('APP_TITLE','Localhost')}} CPanel</a> by <a href="highlysucceed.com" target="_blank">Highly Succeed Inc.</a>
</div>
<!-- /footer -->