
@extends('backoffice._layouts.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="glyphicon glyphicon-picture"></i> <span class="text-semibold">About</span> - Edit About Techreport.</h4>
        </div>
        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="{{route('backoffice.faq.index')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack text-primary"></i><span>All Data</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href=""><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="">Techreport.ph</a></li>
            <li class="active">Edit</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <form id="target" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">About Details</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li> -->
                        <!-- <li><a data-action="close"></a></li> -->
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                
                <p class="content-group-lg">Below are the general information for about.</p>

                <div class="form-group {{$errors->first('about') ? 'has-error' : NULL}}">
                    <label for="type" class="control-label col-lg-2 text-right">About<span class="text-danger"></span></label>
                    <div class="col-lg-9">
                        <textarea class="summernote" name="about" id="about" placeholder="">{!!old('about',$about->about)!!}</textarea>
                        @if($errors->first('about'))
                        <span class="help-block">{{$errors->first('about')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{$errors->first('address') ? 'has-error' : NULL}}">
                    <label for="type" class="control-label col-lg-2 text-right">Address </label>
                    <div class="col-lg-9">
                        <input class="fdropup form-control daterange-single" value="{{old('address',$about->address)}}" placeholder="" type="text" name="address">
                        @if($errors->first('address'))
                        <span class="help-block">{{$errors->first('address')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{$errors->first('email') ? 'has-error' : NULL}}">
                    <label for="type" class="control-label col-lg-2 text-right">Email </label>
                    <div class="col-lg-9">
                        <input class="fdropup form-control daterange-single" value="{{old('email',$about->email)}}" placeholder="" type="text" name="email">
                        @if($errors->first('email'))
                        <span class="help-block">{{$errors->first('email')}}</span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group {{$errors->first('contact') ? 'has-error' : NULL}}">
                    <label for="type" class="control-label col-lg-2 text-right">Contact Number </label>
                    <div class="col-lg-9">
                        <input class="fdropup form-control daterange-single" value="{{old('contact',$about->contact)}}" placeholder="" type="text" name="contact">
                        @if($errors->first('contact'))
                        <span class="help-block">{{$errors->first('contact')}}</span>
                        @endif
                    </div>
                </div>
                
                

            </div>
        </div>

        <div class="content-group">
            <div class="text-left">
                <button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Saving ..." class="btn btn-primary btn-raised btn-lg btn-loading">Save</button>
                &nbsp;
                <a type="button" class="btn btn-default btn-raised btn-lg" href="{{route('backoffice.users.index')}}">Cancel</a>
            </div>
        </div>
    </form>
    @include('backoffice._components.footer')
</div>
<!-- /content area -->
@stop
@section('modals')
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/spin.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/ladda.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>

<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/pages/form_inputs.js')}}"></script>

<!-- Daterange Picker -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/pickers/daterangepicker.js')}}"></script>

<!-- CKEditor -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/editors/ckeditor/ckeditor.js')}}"></script>
<link href="{{ asset('backoffice/summernote/summernote.css')}}" rel="stylesheet"> 
<script type="text/javascript" src="{{asset('backoffice/js/plugins/editors/summernote/summernote.min.js')}}"></script>
<script type="text/javascript">     
    $(function(){           
        $(".summernote").summernote({ 
            height : 300,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear', 'superscript', 'subscript']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table',['table']],
                ['insert',['link','picture','video']],
                ['view',[/*'fullscreen',*/'codeview','help','undo','redo']]
            ],
            fontNames:["Arial","Arial Black","Comic Sans MS","Courier New","Helvetica Neue","Helvetica","Impact","Lucida Grande","Tahoma","Times New Roman","Verdana","Roboto"],

            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            },
            cleaner:{
                 notTime: 2400, // Time to display Notifications.
                 action: 'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
                 newline: '<br>', // Summernote's default is to use '<p><br></p>'
                 notStyle: 'position:absolute;top:0;left:0;right:0', // Position of Notification
                 icon: '<i class="note-icon">Reset</i>',
                 keepHtml: false, // Remove all Html formats
                 keepClasses: false, // Remove Classes
                 badTags: ['style', 'script', 'applet', 'embed', 'noframes', 'noscript', 'html'], // Remove full tags with contents
                 badAttributes: ['style', 'start'] // Remove attributes from remaining tags
            },
            dialogsFade: true,
            dialogsInBody: true,
            placeholder: 'Content'
        }); 

        function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            data.append("api_token","{{env('APP_KEY')}}");
            $.ajax({
                data: data,
                type: "POST",
                url: "{{route('api.summernote',['json'])}}",
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                 if(data.status == true){
                  $('.summernote').summernote('insertImage', data.image);
                 }
                }
            });
        }       
    }); 
</script>

@stop

 