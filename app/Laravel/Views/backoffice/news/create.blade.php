@extends('backoffice._layouts.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-newspaper"></i> <span class="text-semibold">News</span> - Create a new news.</h4>
        </div>
        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="{{route('backoffice.news.index')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack text-primary"></i><span>All Data</span></a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href=""><i class="icon-home2 position-left"></i> Home</a></li>
            <li><a href="">News</a></li>
            <li class="active">Create</li>
        </ul>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
    <form id="target" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">News Details</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li> -->
                        <!-- <li><a data-action="close"></a></li> -->
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                
                <p class="content-group-lg">Below are the general information for this user.</p>

                <div class="form-group {{$errors->first('title') ? 'has-error' : NULL}}">
                    <label for="type" class="control-label col-lg-2 text-right">Title<span class="text-danger"></span></label>
                    <div class="col-lg-9">
                        <input type="text" id="title" name="title" class="dropup form-control daterange-single" placeholder="Title" value="{{old('title')}}">
                        @if($errors->first('title'))
                        <span class="help-block">{{$errors->first('title')}}</span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group {{$errors->first('author') ? 'has-error' : NULL}}">
                    <label for="type" class="control-label col-lg-2 text-right">Author </label>
                    <div class="col-lg-9">
                        <input class="fdropup form-control daterange-single" value="{{old('author')}}" placeholder="Enter Author" type="text" name="author">
                        @if($errors->first('author'))
                        <span class="help-block">{{$errors->first('author')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{$errors->first('category') ? 'has-error' : NULL}}">
                    <label for="type" class="control-label col-lg-2 text-right">Category<span class="text-danger"></span></label>
                    <div class="col-lg-9">
                        {!!Form::select("category", $category, old('category'), ['id' => "category", 'class' => "form-control input-sm"]) !!}
                        @if($errors->first('category'))
                        <span class="help-block">{{$errors->first('category')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{$errors->first('is_featured') ? 'has-error' : NULL}}">
                    <label for="type" class="control-label col-lg-2 text-right">Featured<span class="text-danger"></span></label>
                    <div class="col-lg-9">
                        {!!Form::select("is_featured", $featureds, old('is_featured'), ['id' => "is_featured", 'class' => "form-control input-sm"]) !!}
                        @if($errors->first('is_featured'))
                        <span class="help-block">{{$errors->first('is_featured')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{$errors->first('content') ? 'has-error' : NULL}}">
                    <label for="content" class="control-label col-lg-2 text-right">Content</label>
                    <div class="col-lg-9">
                        <textarea class="summernote" value="" placeholder="Enter Content type" name="content">{{old('content')}}</textarea>
                        <span class="help-block">This field is optional.</span>
                        @if($errors->first('content'))
                        <span class="help-block">{{$errors->first('content')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{$errors->first('posted_at') ? 'has-error' : NULL}}">
                    <label for="content" class="control-label col-lg-2 text-right">Posted At</label>
                    <div class="col-lg-9">
                        <input class="fdropup form-control daterange-single" value="{{old('posted_at')}}" placeholder="Enter Posted At" type="date" name="posted_at">
                        <span class="help-block">This field is optional.</span>
                        @if($errors->first('posted_at'))
                        <span class="help-block">{{$errors->first('posted_at')}}</span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{$errors->first('file') ? 'has-error' : NULL}}">
                    <label class="control-label col-lg-2 text-right">Upload avatar</label>
                    <div class="col-lg-9">
                        <input type="file" name="file" class="file-styled-primary" data-multiple-caption="{count} files selected" multiple="" class="inputfile">
                        @if($errors->first('file'))
                        <span class="help-block">{{$errors->first('file')}}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="content-group">
            <div class="text-left">
                <button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Saving ..." class="btn btn-primary btn-raised btn-lg btn-loading">Save</button>
                &nbsp;
                <a type="button" class="btn btn-default btn-raised btn-lg" href="{{route('backoffice.users.index')}}">Cancel</a>
            </div>
        </div>
    </form>
    @include('backoffice._components.footer')
</div>
<!-- /content area -->
@stop
@section('modals')
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/spin.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/editors/summernote/summernote.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/ladda.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>

<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/pages/form_inputs.js')}}"></script>

<!-- Daterange Picker -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/pickers/daterangepicker.js')}}"></script>

<!-- CKEditor -->
<link href="{{ asset('backoffice/summernote/summernote.css')}}" rel="stylesheet"> 
<script src="{{ asset('backoffice/summernote/summernote.min.js')}}"></script> 
{{-- <script src="{{asset('frontend/summernote/summernote-cleaner.js')}}"></script> --}}
<script type="text/javascript">     
    $(function(){           
        $(".summernote").summernote({ 
            height : 300,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear', 'superscript', 'subscript']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table',['table']],
                ['insert',['link','picture','video']],
                ['view',[/*'fullscreen',*/'codeview','help','undo','redo']]
            ],
            fontNames:["Arial","Arial Black","Comic Sans MS","Courier New","Helvetica Neue","Helvetica","Impact","Lucida Grande","Tahoma","Times New Roman","Verdana","Roboto"],

            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            },
            cleaner:{
                 notTime: 2400, // Time to display Notifications.
                 action: 'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
                 newline: '<br>', // Summernote's default is to use '<p><br></p>'
                 notStyle: 'position:absolute;top:0;left:0;right:0', // Position of Notification
                 icon: '<i class="note-icon">Reset</i>',
                 keepHtml: false, // Remove all Html formats
                 keepClasses: false, // Remove Classes
                 badTags: ['style', 'script', 'applet', 'embed', 'noframes', 'noscript', 'html'], // Remove full tags with contents
                 badAttributes: ['style', 'start'] // Remove attributes from remaining tags
            },
            dialogsFade: true,
            dialogsInBody: true,
            placeholder: 'Content'
        }); 

        function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            data.append("api_token","{{env('APP_KEY')}}");
            $.ajax({
                data: data,
                type: "POST",
                url: "{{route('api.summernote',['json'])}}",
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                 if(data.status == true){
                  $('.summernote').summernote('insertImage', data.image);
                 }
                }
            });
        }       
    }); 
</script>
@stop

 

 