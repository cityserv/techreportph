      <nav id="menu-1" class="mega-menu">
         <section class="menu-list-items">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12 col-md-12">
                     <ul class="menu-logo">
                        <li>
                           <a href="index-2.html"><img id="logo_img" src="{{asset($logo->directory.'/'.$logo->filename)}}" alt="logo" style= "margin-top: -1px;"> </a>
                        </li>
                     </ul>
                     <ul class="menu-links">
                        <li>
                           <a href="{{route('frontend.index')}}" style= "margin-top: 5px;">Home</a>
                        </li>
                        <li><a href="{{route('frontend.about')}}" style= "margin-top: 5px;"> About </a></li>
                        <li>
                           <a href="javascript:void(0)" style= "margin-top: 5px;">News<i class="fa fa-angle-down fa-indicator"></i></a>
                           <ul class="drop-down-multilevel">
                              @foreach($category as $index => $info)
                              <li><a href="{{route('frontend.news.news',$info->id.'-'.Str::slug($info->title))}}">{{$info->title}}</a></li> 
                              @endforeach
                           </ul>
                        </li>
                        <li>
                           <a href="{{route('frontend.gallery.index')}}" style= "margin-top: 5px;">Gallery</a>
                        </li>

                        <li>
                           <a href="{{route('frontend.faq')}}" style= "margin-top: 5px;">FAQs</a>
                        </li>
                        <li><a href="{{route('frontend.contact')}}" style= "margin-top: 5px;"> Contact Us</a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </section>
      </nav>
      <!-- menu end -->