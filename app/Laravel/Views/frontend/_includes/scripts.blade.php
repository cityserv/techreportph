      <script src="{{asset('frontend/js/modernizr.js')}}"></script>
      <!-- JAVASCRIPT JS  -->
      <script type="text/javascript" src="{{asset('frontend/js/jquery.min.js')}}"></script>
      <!-- BOOTSTRAP CORE JS -->
      <script type="text/javascript" src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
      <!-- JQUERY MIGRATE -->
      <script type="text/javascript" src="{{asset('frontend/js/jquery-migrate.min.js')}}"></script>
      <!-- Jquery Counter -->
      <script src="{{asset('frontend/js/jquery.countTo.js')}}"></script>
      <!-- Jquery Waypoints -->
      <script src="{{asset('frontend/js/jquery.waypoints.js')}}"></script>
      <!-- Jquery Appear Plugin -->
      <script src="{{asset('frontend/js/jquery.appear.min.js')}}"></script>
      <!-- JQUERY SELECT -->
      <script type="text/javascript" src="{{asset('frontend/js/select2.min.js')}}"></script>
      <!-- MEGA MENU -->
      <script type="text/javascript" src="{{asset('frontend/js/megamenu.js')}}"></script>
      <!-- Owl Carousel -->
      <script type="text/javascript" src="{{asset('frontend/js/owl-carousel.js')}}"></script>
      <!-- TYPED JS --> 
      <script src="{{asset('frontend/js/typed.min.js')}}" type="text/javascript"></script>
      <!-- Gallery Magnify  -->
      <script src="{{asset('frontend/js/jquery.magnific-popup.min.js')}}"></script>
      <!-- MASTER SLIDER JS --> 
      <script src="{{asset('frontend/masterslider/js/masterslider.min.js')}}"></script>
      <!-- CORE JS --> 
      <script type="text/javascript" src="{{asset('frontend/js/custom.js')}}"></script>
