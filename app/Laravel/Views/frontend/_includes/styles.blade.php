      <link rel="icon" href="{{asset('frontend/images/techreport1.png')}}" type="image/x-icon">
      <!-- BOOTSTRAPE STYLESHEET CSS FILES -->
      <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.css')}}">
      <!-- JQUERY SELECT -->
      <link href="{{asset('frontend/css/select2.min.css')}}" rel="stylesheet" />
      <!-- JQUERY MENU -->
      <link rel="stylesheet" href="{{asset('frontend/css/megamenu.css')}}">
      <!-- ANIMATION -->
      <link rel="stylesheet" href="{{asset('frontend/css/animate.min.css')}}">
      <!-- OWl  CAROUSEL-->
      <link rel="stylesheet" href="{{asset('frontend/css/owl.carousel.css')}}">
      <link rel="stylesheet" href="{{asset('frontend/css/owl.style.css')}}">
      <link href="{{asset('frontend/css/magnific-popup.css')}}" rel="stylesheet">
      <!-- TEMPLATE CORE CSS -->
      <link rel="stylesheet" href="{{asset('frontend/css/style.css')}}">
      <!-- ICON FONTS -->
      <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/font-awesome.min.css')}}">
      <link rel="stylesheet" href="{{asset('frontend/css/themify-icons.css')}}" type="text/css">
      <link rel="stylesheet" type="text/css" href="{{asset('frontend/fonts/seo-and-marketing/flaticon.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('frontend/fonts/seo-icons/flaticon.css')}}">
      <!--  MASTER SLIDER -->
      <link rel="stylesheet" href="{{asset('frontend/masterslider/css/masterslider.css')}}"/>
      <link href="{{asset('frontend/masterslider/css/style.css')}}" rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="{{asset('frontend/masterslider/css/ms-staff-style.css')}}">
      <link href="{{asset('frontend/masterslider/css/ms-staff-style.css')}}" rel='stylesheet' type='text/css'>
      <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans:300,400,500,700,800" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet" type="text/css">

      <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/structure.css')}}">