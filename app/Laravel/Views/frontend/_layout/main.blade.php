<!DOCTYPE html>
<html lang="en">
   
<head>
   @include('frontend._components.metas')
   @include('frontend._includes.styles')      
   @yield('page-styles')
   </head>
   <body>
         @include('frontend._components.header')
         @yield('content')


         @include('frontend._components.footer')

        <a href="#" class="scrollup"><i class="fa fa-chevron-up"></i></a>

         @include('frontend._includes.scripts')
         @yield('page-scripts')
   </body>
</html>
 