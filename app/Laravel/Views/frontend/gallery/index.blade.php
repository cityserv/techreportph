@extends('frontend._layout.main')

@section('content')
<div class="clearfix"></div>
      <section class="my-breadcrumb parallex">
         <div class="container page-banner">
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12 heading">
                  <div class="main-heading-container">
                     <h1 style="font-size: 50px;"><b>GALLERY</b></h1><br>
                     <h4><i>White Hat is powerful, beautiful, and fully responsive WordPress Theme with multiple options</i></h4>
                  </div>
               </div>
            </div>
         </div>
      </section>
<!-- <div id="page-content" class="page-wrapper">
    <div class="zm-section single-post-wrap bg-white ptb-20 xs-pt-30">
        <div class="container" style="transform: none;">
            <div class="row" style="transform: none;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 columns" style="position: relative; box-sizing: border-box; min-height: 0px;">

                    <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static;">
                        <div class="row mb-40">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="section-title">
                                    <h2 class="h6 header-color inline-block uppercase">ABAC ALBUMS</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="zm-posts clearfix">
                                @foreach($albums as $index => $info)
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                                    <article class="zm-trending-post zm-lay-a1 zm-single-post" data-effict-zoom="1">
                                        <div class="zm-post-thumb">
                                            <a href="{{route('frontend.gallery.show',$info->id.'-'.Str::slug($info->title))}}" data-dark-overlay="2.5" data-scrim-bottom="9">
                                                <img src="{{asset($info->directory.'/'.$info->filename)}}" alt="img">
                                            </a>
                                        </div>
                                        <div class="zm-post-dis text-white">
                                            <h2 class="zm-post-title h3">
                                                <a href="{{route('frontend.gallery.show',$info->id.'-'.Str::slug($info->title))}}">{{$info->title}}</a>
                                            </h2>
                                            <div class="zm-post-meta">
                                                <ul>
                                                    <li class="s-meta">({{count($info->photos)}}) Photos</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
      <section class="blog-posts light-blue">
         <div class="container">
            <div class="row">
               @foreach($albums as $index => $info)
               <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="blog-post">
                     <div class="post-img">
                        <a href="{{route('frontend.gallery.show',$info->id.'-'.Str::slug($info->title))}}"> <img src="{{asset($info->directory.'/'.$info->filename)}}" alt="" class="img-responsive"> </a>
                     </div>
                     <div class="blog-detail">
                        <h3 class="post-title"> <a href="{{route('frontend.gallery.show',$info->id.'-'.Str::slug($info->title))}}">{{$info->title}}({{count($info->title)}})</a></h3>
                     </div>
                  </div>
               </div>
               @endforeach
               
                    <div class="pager">
                        {!!$albums->appends(Input::except('page'))->render()!!}
                        {{-- <ul class="pagination style2">
                          <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                          <li class="selected"><a href="#">1</a></li>
                          <li><a href="#">2</a></li>
                          <li><a href="#">3</a></li>
                          <li><a href="#">4</a></li>
                          <li><a href="#">5</a></li>
                          <li><a href="#">6</a></li>
                          <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                        </ul> --}}
                    </div>
            </div>
         </div>
      </section>
@stop