@extends('frontend._layout.main')

@section('content')
      <div class="clearfix"></div>
      <!-- HOME SLIDER -->
      <section class="slider-container">
         <div class="master-slider ms-skin-default" id="masterslider">
            @foreach($slider as $index => $image)
            <!-- slide 1 -->
            <div class="ms-slide slide-1" data-delay="9">
               <img src="{{asset($image->directory.'/'.$image->filename)}}" data-src="{{asset($image->directory.'/'.$image->filename)}}" alt=""/> 
               <h3 class="ms-layer text2 margin-top"
                  style="left: 110px;top: 200px;"
                  data-type="text"
                  data-delay="2800"
                  data-ease="easeOutExpo"
                  data-duration="1230"
                  data-effect="left(250)">{{$image->title}}</h3>
               <h3 class="ms-layer text3 margin-top text-left"
                  style="left: 110px; top: 300px;"
                  data-type="text"
                  data-effect="top(45)"
                  data-duration="2000"
                  data-delay="3300"
                  data-ease="easeOutExpo">{{$image->sub_title}}
               </h3>
               <a class="ms-layer btn sbut2 white margin-top2"
                  style="left: 110px; top: 380px;"
                  data-type="text"
                  data-delay="3800"
                  data-ease="easeOutExpo"
                  data-duration="1200"
                  data-effect="scale(1.5,1.6)"> Read more </a>
            </div>
            <!-- end slide 1 --> 
            @endforeach
         </div>
         <div class="clearfix"></div>
      </section>
      <section id="about" class="light-blue">
         <div class="container">
            <div class="row">
               <div class="col-md-5 col-sm-6 hidden-sm">
                  <img src="{{asset('frontend/images/slider/weblankan-services-seo.png')}}" class="img-responsive" alt="" />
               </div>
               <div class="col-md-7 col-sm-12 col-xs-12">
                  <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
                     <div class="heading-2">
                        <h3 style="font-size: 25px;"> About</h3>
                        <h2 style="font-size: 50px;">TECHREPORT PH</h2>
                     </div>
                  </div>
                  <p> {{str_limit(strip_tags($about->about), $limit = 500)}}</p>
                  <a href="{{route('frontend.about')}}" class="btn btn-custom white"> Read More </a>
               </div>
            </div>
         </div>
      </section>
      <section id="services">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12 heading">
                  <div class="main-heading-container">
                     <h1 style="font-size: 50px;"><b>Our Services</b></h1>
                  </div>
               </div>
               <div class="col-md-12 col-sm-12 col-xs-12 nopadding">
                  <div class="col-md-4 col-sm-6 col-xs-12">
                     <div class="services-box">
                        <div class="iconbox">
                           <i class="ti-zoom-in"></i>
                           <div class="iconbox-meta">
                              <h4><a href="#">SEO Analysis</a></h4>
                              <p>Seacrh Engine Optimaization</p>
                           </div>
                        </div>
                     </div>
                     <div class="services-box">
                        <div class="iconbox">
                           <i class="ti-hand-point-up"></i>
                           <div class="iconbox-meta">
                              <h4><a href="#">Pay Per Click</a></h4>
                              <p>Seacrh Engine Optimaization</p>
                           </div>
                        </div>
                     </div>
                     <div class="services-box">
                        <div class="iconbox">
                           <i class="ti-location-arrow"></i>
                           <div class="iconbox-meta">
                              <h4><a href="#">SEO Link Building</a></h4>
                              <p>Seacrh Engine Optimaization</p>
                           </div>
                        </div>
                     </div>
                     <div class="services-box">
                        <div class="iconbox">
                           <i class="ti-blackboard"></i>
                           <div class="iconbox-meta">
                              <h4><a href="#">Website Analytics</a></h4>
                              <p>Seacrh Engine Optimaization</p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                     <div class="services-box">
                        <div class="iconbox">
                           <i class="ti-eye"></i>
                           <div class="iconbox-meta">
                              <h4><a href="#">Site Monitering</a></h4>
                              <p>Seacrh Engine Optimaization</p>
                           </div>
                        </div>
                     </div>
                     <div class="services-box">
                        <div class="iconbox">
                           <i class="ti-layout-grid2-thumb"></i>
                           <div class="iconbox-meta">
                              <h4><a href="#">SEO Site Maping</a></h4>
                              <p>Seacrh Engine Optimaization</p>
                           </div>
                        </div>
                     </div>
                     <div class="services-box">
                        <div class="iconbox">
                           <i class="ti-bar-chart"></i>
                           <div class="iconbox-meta">
                              <h4><a href="#">Charts Ranking</a></h4>
                              <p>Seacrh Engine Optimaization</p>
                           </div>
                        </div>
                     </div>
                     <div class="services-box">
                        <div class="iconbox">
                           <i class="ti-headphone"></i>
                           <div class="iconbox-meta">
                              <h4><a href="#">premium Support</a></h4>
                              <p>Seacrh Engine Optimaization</p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-12 hidden-sm">
                     <img src="images/tablet.png" class="img-responsive" alt="" />
                  </div>
               </div>
            </div>
         </div>
          </div><center><a href="services.html" class="btn btn-custom white" > Read More </a></center></div>
      </section>

      <section class="testimoniial-section light-blue" >
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12 heading">
                  <div class="main-heading-container">
                     <h1 style="font-size: 50px;"><b>TESTIMONIALS</b></h1>
                     <p>See what our beloved clients say. </p>
                  </div> <br>
               <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="owl-testimonial-2">
                     @foreach($testimonials as $index => $info)
                     <div class="single_testimonial">
                        <div class="textimonial-content">
                           <!-- <h4>{{$info->title}}</h4> -->
                           <p>{{$info->message}}</p>
                        </div>
                        <div class="testimonial-meta-box">
                           <img src="{{asset($info->directory.'/'.$info->filename)}}" alt="">
                           <div class="testimonial-meta">
                              <h3 class="">{{$info->author}}</h3>
                              <p>{{$info->position}}</p>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                           </div>
                        </div>
                     </div>
                     @endforeach
                  </div>
               </div>
            </div>
               </div>
         </div>
         </div><center><a href="services.html" class="btn btn-custom white" > Read More </a></center></div>
      </section>
      
      <section class="project-container">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12 heading">
                  <div class="main-heading-container">
                     <h1 style="font-size: 50px;"><b>LATEST NEWS</b></h1><br>
                     <p>White Hat is powerful, beautiful, and fully responsive WordPress Theme with multiple options</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="project-container">
            <div class="owl-carousel owl-theme recent-project-carousel">
                  @foreach($news as $index => $info)
                  <div class="blog-post">
                     <div class="post-img">
                        <a href="#"> <img src="{{asset($info->directory.'/'.$info->filename)}}" alt="" class="img-responsive"> </a>
                     </div>
                     <div class="blog-detail">
                        <h3 class="post-title"> <a href="#">{{$info->title}}</a> </h3>
                        <p class="post-excerpt">{{str_limit(strip_tags($info->content), $limit = 100)}} </p> <br>
                       <left> <a href="#" class="btn btn-custom white"> Read More</a> </left>
                     </div>
                     <div class="blog-meta">
                        <span class="pull-left"><a href="#"><i class="fa fa-commenting"></i>25 Comments</a></span>
                        <span class="pull-right"> <a href="#" data-toggle="tooltip" data-placement="top" title="Admin"><img src="images/users/1.jpg" alt=""></a></span>
                     </div>
                  </div>
                  @endforeach
               </div>
            </div>
         </div>
      </section>
      
      <section class="project-container-2" style="background-color: #D0E4F9">
         <div class="container">
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12 heading">
                  <div class="main-heading-container">
                     <h1 style="font-size: 50px;"><b>Our Partners</b></h1> <br>
                     <p>White Hat is powerful, beautiful, and fully responsive WordPress Theme with multiple options</p>
                  </div>
               </div>
               @foreach($partners as $index => $info)
               <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="recent-project">
                     <a href="#"><img src="{{asset($info->directory.'/'.$info->filename)}}" alt=""></a>
                     <div class="project-info">
                        <h3><a href="#">{{$info->title}}</a></h3>
                     </div>
                  </div>
               </div>
               @endforeach
            </div>
         </div>
          </div><center><a href="services.html" class="btn btn-custom white" > Read More </a></center></div>
      </section>
@stop