@extends('frontend._layout.main')
@section('content')
      <div class="clearfix"></div>
      <section class="my-breadcrumb parallex">
         <div class="container page-banner">
            <div class="row">
               <div class="col-sm-6 col-md-6">
                  <h1>NEWS</h1>
               </div>
            </div>
         </div>
      </section>
      <section class="blog-posts">
         <div class="container">
            <div class="row">
               <div class="col-md-8 col-sm-12 col-xs-12">
                  <img src="{{asset($current_news->directory.'/'.$current_news->filename)}}" alt="" class="img-responsive">
                  <div class="single-listing-page-meta-box">
                     <div class="single-listing-page-meta">
                        <h2>{{$current_news->title}}</h2>
                        <p> <span><i class="fa fa-calendar-o"></i>{{Helper::date_format($current_news->posted_at,'F d, Y')}}</span> <span><i class="fa fa-comment-o"></i><a href=" #disqus_thread">35</a></span> <span><i class="fa fa-user"></i>{{$current_news->author}}</span> </p>
                     </div>
                     <!--<a class="btn btn-default pull-right" href="">Write A Review</a> </div>-->
                     <div class="single-listing-page-desc">
                        <h4>Description</h4>
                        <p>{!!strip_tags($current_news->content)!!}</p>                       
                     </div>
                  </div>
                  <div id="disqus_thread"></div>
               </div>

               <div class="col-md-4 col-sm-4 col-xs-12">
                  <aside>
                     <div class="widget">
                        <div class="search">
                           <div class="input-group stylish-input-group">
                              <input type="text" placeholder="Search" class="form-control">
                              <span class="input-group-addon">
                              <button type="submit"> <span class="fa fa-search"></span> </button>
                              </span> 
                           </div>
                        </div>
                     </div>
                     <div class="widget">
                        <h4>Recent Blogs</h4>
                        @foreach($news as $index => $info)
                        <div class="single-pp-wrapper">
                           <div class="popular-post-img"> <a href="#"><img src="{{asset($info->directory.'/'.$info->filename)}}" alt=""></a> </div>
                           <div class="popular-post-content">
                              <h5 class="post-title"><a href="{{route('frontend.news.show',$info->id.'-'.Str::slug($info->title))}}">{{$info->title}}</a></h5>
                              <p>On {{Helper::date_format($info->posted_at,'F d, Y')}}</p>
                           </div>
                        </div>
                        @endforeach
                     </div>
                  </aside>
               </div>
            </div>
            
                  <script>             
                     var disqus_config = function () {
                        this.page.url = '{{Request::url()}}';
                        this.page.identifier = {{$current_news->id}}; 
                     };
                                
                     (function() {
                        var d = document, s = d.createElement('script');
                        s.src = 'https://techreportph.disqus.com/embed.js';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                        })();
                  </script>
                     <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                            
                     <script id="dsq-count-scr" src="//techreportph.disqus.com/count.js" async></script>

         </div>
      </section>
@stop
