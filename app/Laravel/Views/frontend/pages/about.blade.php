@extends('frontend._layout.main')
@section('content')
      <div class="clearfix"></div>
      <section class="my-breadcrumb parallex">
            <div class="container page-banner">
               <div class="row">
                  <div class="col-sm-6 col-md-6">
                     <h4>Who we are</h4>
                     <h1>About Us</h1>
                  </div>
               </div>
            </div>
         </section>
         <section class="services-detail">
            <div class="container">
               <div class="row">
                  <div class="col-md-12 col-xs-12 col-sm-12 nopadding">
                     <div class="col-md-7 col-sm-12 col-xs-12">
                        <div id="post-slider" class="owl-carousel owl-theme margin-bottom-30">
                           @foreach($slider as $index => $image)
                           <div class="item">
                              <a class="tt-lightbox" href="{{asset($image->directory.'/'.$image->filename)}}"><img class="img-responsive" src="{{asset($image->directory.'/'.$image->filename)}}" alt=""></a>
                           </div>
                           @endforeach
                        </div>
                     </div>
                     <div class="col-md-5 col-sm-12 col-xs-12">
                        <div class="project-overview no-padding">
                           <h2>About <b>TECHREPORT PH</b></h2>
                           <p>{{strip_tags($about->about)}}</p>
                        </div>
                     </div>
                  </div>
               </div> <hr>
         </div>
         <section class="project-container-2" style="background-color: #f4f7fa;">
            <div class="container">
               <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12 heading">
                     <div class="main-heading-container">
                       <h1 style="font-size: 50px;"><b>OUR PARTNERS</b></h1>
                        <p>White Hat is powerful, beautiful, and fully responsive WordPress Theme with multiple options.</p>
                     </div>
                  </div>
               @foreach($partners_1 as $index => $info)
               <div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="recent-project">
                     <a href="#"><img src="{{asset($info->directory.'/'.$info->filename)}}" alt=""></a>
                     <div class="project-info">
                        <h3><a href="#">{{$info->title}}</a></h3>
                     </div>
                  </div>
               </div>
               @endforeach
               </div>
            </div>
         </section>
         </section>

@stop