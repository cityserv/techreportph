<?php

use App\Laravel\Models\About;
use Illuminate\Database\Seeder;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	
	About::create(
	    ['about' => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti veniam aperiam blanditiis debitis, odio qui fugiat rerum placeat quis sint nostrum quam ullam doloremque assumenda tempore enim saepe, error repellendus.",
        'address' => "Makati City",
        'email' => "admin@highlysucceed.com",
        'contact' => "09222222222"
	    ]
	);

    }
}
