$(function(){
	$('.daterange-single').daterangepicker({ 
		singleDatePicker: true,
		timePicker: true,
		timePickerIncrement: 5,
		locale: {
			format: 'YYYY-MM-DD h:mm A'
		}
	});
});