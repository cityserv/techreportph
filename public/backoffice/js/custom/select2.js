$(function(){
	$('.select').each(function(){
		$id = "#" + $(this).attr('id') + " option:first";
		$($id).prop('disabled',1);
	});

	$('.select').select2({
		minimumResultsForSearch: Infinity
	});
});